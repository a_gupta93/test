class DemoController < ApplicationController
  def inde
  end
  
  def new
	render('inde')
  #---------------------------------------------------------------------------------------------------------------------------------------------------------
  #This area grabs the input from the user
  params.permit!
  city = params[:city].humanize
  category = params[:category]
  area = params[:area]
  #----------------------------------------------------------------------------------------------------------------------------------------------------------
  #This area inserts hifen(-) between words as required to make the url
  map = {' ' => '-'}
  numofwords = 0
  while numofwords < category.split.size do
  map.each{|k,v| category.sub!(k,v)}
  numofwords = numofwords + 1
  end
  #the below if statement executes only when area is specified
  if area != nil
  numofwords = 0
  while numofwords < area.split.size do
  map.each{|k,v| area.sub!(k,v)}
  numofwords = numofwords + 1
  end
  end
  
  time = Time.new
  #-----------------------------------------------------------------------------------------------------------------------------------------------------------
  require 'nokogiri'
  require 'open-uri'
  require 'rubygems'
  require 'spreadsheet'
  #--------------------------------------------------------------------------------------------------------------------------------------------------------
  #initializing the spreadsheet
  Spreadsheet.client_encoding = 'UTF-8'
  book = Spreadsheet::Workbook.new
  sheet1 = book.create_worksheet
  if area != nil
	temp = Nokogiri::HTML(open("http://www.justdial.com/#{city}/#{category}-%3Cnear%3E-#{area}"))
  else
	temp = Nokogiri::HTML(open("http://www.justdial.com/#{city}/#{category}"))
  end
  tempcode = temp.css('div#srchpagination a')
  if tempcode.length == 0
	section = temp.css('section.jrcl')
	count = section.length
	test = 0
	while test < 25 do
	test = test + 1
	end
	sleep 10
	j = 0
	sql = "CREATE TABLE #{tablename} (city varchar(20) NOT NULL, name varchar(200) NOT NULL, phone_number varchar(20) NOT NULL,address varchar(500) NOT NULL, rating float NOT NULL)"
	#ActiveRecord::Base.connection.execute(sql)
	while j < count do
		names = section[j].css('span.jcn a')
		phone = section[j].css('p.jrcw a')
		address = section[j].css('span.jaddt')
		ratings = section[j].css('span.star_m span')
		r10 = ratings.css('span.s10')
		r9 = ratings.css('span.s9')
		r8 = ratings.css('span.s8')
		r7 = ratings.css('span.s7')
		r6 = ratings.css('span.s6')
		r5 = ratings.css('span.s5')
		r4 = ratings.css('span.s4')
		r3 = ratings.css('span.s3')
		r2 = ratings.css('span.s2')
		r1 = ratings.css('span.s1')
		net_rating = (r10.length*1)+(r9.length*0.9)+(r8.length*0.8)+(r7.length*0.7)+(r6.length*0.6)+(r5.length*0.5)+(r4.length*0.4)+(r3.length*0.3)+(r2.length*0.2)+(r1.length*0.1)
		if address[0] == nil
			add = "NA"
		else
			add = address[0].text
			
		end
		if phone[0] == nil
			ph = "NA"
		else
			ph = phone[0].text
		end
		sql = "INSERT INTO #{tablename} VALUES('#{city}','#{names[0]["title"]}','#{ph}','#{add}','#{net_rating}')"
		#ActiveRecord::Base.connection.execute(sql)
		sheet1.row(j).push city, names[0]["title"], ph, add, net_rating
		j = j + 1
		names.pop
		phone.pop
		address.pop
	end
	sleep 5
	book.write "#{city}_#{category}"+time.day.to_s+"-"+time.month.to_s+"-"+time.year.to_s+"_"+time.hour.to_s+time.min.to_s+time.sec.to_s+".xls"
  else 
	page2 = tempcode[1]["href"]
  map = {'<' => '%3C', '>' => '%3E'}
  numofwords = 0
  while numofwords < page2.split.size do
  map.each{|k,v| page2.sub!(k,v)}
  numofwords = numofwords + 1
  end
  puts page2
  base_url = page2.chomp('2')
  number = Nokogiri::HTML(open("#{page2}"))
  pages = number.css('div#srchpagination a')
  numofpage = pages[pages.length - 2].text.to_i
  if numofpage == 1 
	numofpage = 2
  end
  puts numofpage
  tablename = "#{city}_#{category}"+time.day.to_s+time.month.to_s+time.year.to_s+"_"+time.hour.to_s+time.min.to_s+time.sec.to_s
  sql = "CREATE TABLE #{tablename} (city varchar(20) NOT NULL, name varchar(200) NOT NULL, phone_number varchar(20) NOT NULL,address varchar(500) NOT NULL, rating float NOT NULL)"
  #ActiveRecord::Base.connection.execute(sql)
  i=1
  itr=0
  while i <= numofpage do 
	puts "#{base_url}"+"#{i}"
	doc = Nokogiri::HTML(open("#{base_url}"+"#{i}"))
	section = doc.css('section.jrcl')
	count = section.length
	test = 0
	while test < 25 do
	test = test + 1
	end
	sleep 10
	j = 0
	while j < count do
		names = section[j].css('span.jcn a')
		phone = section[j].css('p.jrcw a')
		address = section[j].css('span.jaddt')
		ratings = section[j].css('span.star_m span')
		r10 = ratings.css('span.s10')
		r9 = ratings.css('span.s9')
		r8 = ratings.css('span.s8')
		r7 = ratings.css('span.s7')
		r6 = ratings.css('span.s6')
		r5 = ratings.css('span.s5')
		r4 = ratings.css('span.s4')
		r3 = ratings.css('span.s3')
		r2 = ratings.css('span.s2')
		r1 = ratings.css('span.s1')
		net_rating = (r10.length*1)+(r9.length*0.9)+(r8.length*0.8)+(r7.length*0.7)+(r6.length*0.6)+(r5.length*0.5)+(r4.length*0.4)+(r3.length*0.3)+(r2.length*0.2)+(r1.length*0.1)
		if address[0] == nil
			add = "NA"
		else
			add = address[0].text
			
		end
		if phone[0] == nil
			ph = "NA"
		else
			ph = phone[0].text
		end
		add.gsub! /\t/, ''
		add.strip
		puts add;
		sql = "INSERT INTO #{tablename} VALUES('#{city}','#{names[0]["title"]}','#{ph}','#{add}','#{net_rating}')"
		#ActiveRecord::Base.connection.execute(sql)
		sheet1.row(itr).push city, names[0]["title"], ph, add, net_rating
		itr = itr + 1
		j = j + 1
		names.pop
		phone.pop
		address.pop
	end
	i = i + 1
	sleep 5
	book.write "#{city}_#{category}"+time.day.to_s+"-"+time.month.to_s+"-"+time.year.to_s+"_"+time.hour.to_s+time.min.to_s+time.sec.to_s+".xls"
	end 
	
  end
  end
end
